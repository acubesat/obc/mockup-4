#include <AX5043.h>
#include "FreeRTOS.h"
#include <freertos/include/task.h>
#include <mockup.h>
#include "AX5043Task.h"
#include "main.h"

void AX5043Task::operator()() {
    xSemaphoreTake(xSPIMutex, portMAX_DELAY);
    AX5043 ax5043(&hspi1, GPIOB, GPIO_PIN_5);

    ax5043.enterReceiveMode();

    xSemaphoreGive(xSPIMutex);

    while (true) {
        vTaskDelay(10);
    }
}
