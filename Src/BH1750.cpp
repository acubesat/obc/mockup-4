#include "FreeRTOS.h"
#include <freertos/include/task.h>
#include "BH1750.h"
#include "main.h"

void BH1750_WriteCommand(uint8_t comm)
{
    uint8_t txData[1];  // data to be transmitted
    txData[0] = comm;   // register address

//    vTaskSuspendAll();
    HAL_I2C_Master_Transmit(&hi2c1, BH1750_ADDR, txData, 1, 100);
//    xTaskResumeAll();
}

uint16_t BH1750_ReadWord(void)
{
    uint16_t rcvData = 0;
    HAL_I2C_Master_Receive(&hi2c1, BH1750_ADDR, reinterpret_cast<uint8_t*>(&rcvData), 2, 300);

    return ((rcvData >> 8U) & 0xff) | ((rcvData & 0xff) << 8U);
}

void BH1750_Init(uint8_t resMode)
{
    //TWIInit(); //Initialize the I2C interface

    BH1750_WriteCommand(BH1750_POWERON);
    BH1750_WriteCommand(BH1750_RESET);
    BH1750_WriteCommand(resMode);
}

double BH1750_GetBrightnessSingle(void)
{
    uint16_t level = 0;

    BH1750_WriteCommand(BH1750_ONELRES);
    HAL_Delay(20); //Wait for data acquisition

    //Receive data value
    level = BH1750_ReadWord(); //Read the measured value of brightness
    level /= 1.2; //Convert bits to lux

    return level;
}

double BH1750_GetBrightnessCont(void)
{
    double level = 0;

    //Receive data value
    level = static_cast<double>(BH1750_ReadWord()); //Read the measured value of brightness
    level /= 1.2; //Convert bits to lux

    return level;
}
