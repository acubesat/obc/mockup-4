#include <cstdio>
#include <inc/ServicePool.hpp>
#include <inc/Message.hpp>
#include <inc/Logger.hpp>
// FreeRTOS includes
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include <UARTTask.h>
#include <MCP9808Task.h>
#include <LEDStripTask.h>
#include <BH1750Task.h>
#include <MPU9250Task.h>
#include <AT86RF233Task.h>
#include <ECSSObjects.h>
#include <ECSSPeriodicTask.h>
#include <AX5043Task.h>
#include <TaskListTask.h>

std::optional<UARTTask> uartTask;
std::optional<MCP9808Task> mcp9808InternalTask;
std::optional<MCP9808Task> mcp9808ExternalTask;
std::optional<LEDStripTask> ledStripTask;
std::optional<BH1750Task> bh1750Task;
std::optional<MPU9250Task> mpu9250Task;
std::optional<AT86RF233Task> at86rf233Task;
std::optional<AX5043Task> ax5043Task;
std::optional<ECSSPeriodicTask> ecssPeriodicTask;
std::optional<TaskListTask> taskListTask;

SemaphoreHandle_t xI2CMutex;
SemaphoreHandle_t xSPIMutex;

/**
 * The queue for all TM Space Packets
 */
QueueHandle_t xECSSQueueHandle;
static StaticQueue_t xECSSQueue;
/**
 * The queue for received TC Space Packets
 */
QueueHandle_t xTCQueueHandle;
static StaticQueue_t xTCQueue;

StaticSemaphore_t i2cMutexStorage;
StaticSemaphore_t spiMutexStorage;

TaskHandle_t xFadeTaskHandle;

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
extern "C" {
#include "main.h"

extern ADC_HandleTypeDef hadc1;


float getTemperature() {
    HAL_ADC_Start(&hadc1);
    HAL_ADC_PollForConversion(&hadc1, 1000);
    uint32_t value = HAL_ADC_GetValue(&hadc1);
    return __HAL_ADC_CALC_TEMPERATURE(3300, value, ADC_RESOLUTION_12B);
}

Message message(1, 1, Message::TM, 0);

void vHeartbeatTask(void *pvParameters) {
    for (;;) {
        LOG_DEBUG << "Heartbeat";

        led_toggle("unused");
        vTaskDelay(pdMS_TO_TICKS(10));

        led_toggle("unused");
        vTaskDelay(pdMS_TO_TICKS(1000));
    }
}


void vFadeTask(void *pvParameters) {
    for (;;) {
        vTaskDelay(pdMS_TO_TICKS((100)));
        vTaskDelay(pdMS_TO_TICKS(7000));
        ledStripTask->fadeInto(colours1);
//
        vTaskDelay(pdMS_TO_TICKS(4000));
        ledStripTask->fadeInto(colours2);
//
        vTaskDelay(pdMS_TO_TICKS(4000));
        ledStripTask->fadeInto(colours3);
//
//        vTaskDelay(pdMS_TO_TICKS(7000));
//        ledStripTask->fadeInto(colours4);


//        vTaskDelay(pdMS_TO_TICKS(7000));
//        ledStripTask->fadeInto(colours5);

        vTaskDelay(pdMS_TO_TICKS(1000));
        ledStripTask->fadeInto(coloursO);
        vTaskDelay(pdMS_TO_TICKS(2000));
        ledStripTask->parseRGB(colours6);
        vTaskDelay(pdMS_TO_TICKS(4));
        ledStripTask->parseRGB(coloursO);

        vTaskDelay(pdMS_TO_TICKS(2000));
        ledStripTask->parseRGB(coloursG);
        vTaskDelay(pdMS_TO_TICKS(8));
        ledStripTask->parseRGB(coloursO);
        vTaskDelay(pdMS_TO_TICKS(200));
        ledStripTask->parseRGB(coloursB);
        vTaskDelay(pdMS_TO_TICKS(8));
        ledStripTask->parseRGB(coloursO);

        vTaskDelay(pdMS_TO_TICKS(1000));
        ledStripTask->parseRGB(coloursG);
        vTaskDelay(pdMS_TO_TICKS(8));
        ledStripTask->parseRGB(coloursO);
        vTaskDelay(pdMS_TO_TICKS(200));
        ledStripTask->parseRGB(coloursB);
        vTaskDelay(pdMS_TO_TICKS(8));
        ledStripTask->parseRGB(coloursO);

        vTaskDelay(pdMS_TO_TICKS(1000));
        ledStripTask->parseRGB(coloursG);
        vTaskDelay(pdMS_TO_TICKS(8));
        ledStripTask->parseRGB(coloursO);
        vTaskDelay(pdMS_TO_TICKS(200));
        ledStripTask->parseRGB(coloursB);
        vTaskDelay(pdMS_TO_TICKS(8));
        ledStripTask->parseRGB(coloursO);

        vTaskDelay(pdMS_TO_TICKS(1000));

        ledStripTask->fadeInto(colours4);
        vTaskDelay(ledStripTask->fadeDuration / 2.0);
        ledStripTask->setCurrentEffect(LEDStripTask::GradualColorMode);



//        vTaskDelay(pdMS_TO_TICKS(400));
//        ledStripTask->setCurrentEffect(LEDStripTask::GradualColorMode);
    }
}


/**
 * Just calls the operator() function of a task
 * @param pvParameters Pointer to object of type Task
 */
void vClassTask(void *pvParameters) {
    (static_cast<Task *>(pvParameters))->operator()();
}

static constexpr size_t ecssQueueSize = 40;
uint8_t ucECSSQueueStorageArea[ ecssQueueSize * sizeof(Message) ];
uint8_t ucTCQueueStorageArea[ ecssQueueSize * sizeof(Message) ];

void main_cpp() {
    uartTask = std::make_optional<UARTTask>();

    LOG_INFO << "Booting FreeRTOS...";

    xI2CMutex = xSemaphoreCreateMutexStatic(&i2cMutexStorage);
    xSPIMutex = xSemaphoreCreateMutexStatic(&spiMutexStorage);

    mcp9808InternalTask = std::make_optional<MCP9808Task>(&hi2c1, 0b000);
    mcp9808ExternalTask = std::make_optional<MCP9808Task>(&hi2c1, 0b111);
    ledStripTask = std::make_optional<LEDStripTask>();
    bh1750Task = std::make_optional<BH1750Task>();
    mpu9250Task = std::make_optional<MPU9250Task>();
    at86rf233Task = std::make_optional<AT86RF233Task>();
    ax5043Task = std::make_optional<AX5043Task>();
    ecssPeriodicTask = std::make_optional<ECSSPeriodicTask>();
    taskListTask = std::make_optional<TaskListTask>();

    xECSSQueueHandle = xQueueCreateStatic(ecssQueueSize, sizeof(Message), ucECSSQueueStorageArea, &xECSSQueue);
    xTCQueueHandle = xQueueCreateStatic(ecssQueueSize, sizeof(Message), ucTCQueueStorageArea, &xTCQueue);

    addECSSObjects(); // Prepare the parameter service

    /* Create any tasks defined within main.c itself, or otherwise specific to the
    demo being built. */
    xTaskCreate(vHeartbeatTask, "heartbeat", 700, nullptr, 4, nullptr);
    xTaskCreate(vClassTask, "UART", 700, &*uartTask, 4, &(uartTask->taskHandle));
    xTaskCreate(vClassTask, "MCP9808_Int", 1200, &*mcp9808InternalTask, 2, nullptr);
    xTaskCreate(vClassTask, "MCP9808_Ext", 1200, &*mcp9808ExternalTask, 2, nullptr);
    xTaskCreate(vClassTask, "BH1750", 1200, &*bh1750Task, 2, nullptr);
    xTaskCreate(vClassTask, "MPU9250", 2400, &*mpu9250Task, 2, nullptr);
    xTaskCreate(vClassTask, "TaskList", 2400, &*taskListTask, 2, nullptr);
    xTaskCreate(vClassTask, "AT86RF233", 4000, &*at86rf233Task, 3, nullptr);
//    xTaskCreate(vClassTask, "AX5043", 4000, &*ax5043Task, 3, nullptr);

    xTaskCreate(vClassTask, "ECSS", 4000, &*ecssPeriodicTask, 3, nullptr);

    xTaskCreate(vClassTask, "LEDStrip", 4000, &*ledStripTask, 1, &(ledStripTask->taskHandle));
    xTaskCreate(vFadeTask, "fade", 1000, nullptr, 5, &xFadeTaskHandle);

    /* Start the RTOS scheduler, this function should not return as it causes the
    execution context to change from main() to one of the created tasks. */
    SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;

    vTaskStartScheduler();
}

/**
  * @brief This function handles DMA2 channel1 global interrupt.
  */
void DMA2_Channel1_IRQHandler(void) {
    /* USER CODE BEGIN DMA2_Channel1_IRQn 0 */
    if (LL_DMA_IsActiveFlag_TC1(DMA2)) {
        LL_DMA_ClearFlag_TC1(DMA2);
        if (uartTask.has_value() && uartTask->taskHandle != nullptr) {
            xTaskNotifyFromISR(uartTask->taskHandle, 0, eNoAction, 0);
        }
    }
    /* USER CODE END DMA2_Channel1_IRQn 0 */

    /* USER CODE BEGIN DMA2_Channel1_IRQn 1 */

    /* USER CODE END DMA2_Channel1_IRQn 1 */
}

/**
 * Implement FreeRTOS's tick function, so that HAL functions that rely on HAL_Delay existing work correctly
 */
void vApplicationTickHook(void) {
    HAL_IncTick();
}

void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer,
                                    StackType_t **ppxIdleTaskStackBuffer,
                                    uint32_t *pulIdleTaskStackSize )
{
/* If the buffers to be provided to the Idle task are declared inside this
function then they must be declared static – otherwise they will be allocated on
the stack and so not exists after this function exits. */
    static StaticTask_t xIdleTaskTCB;
    static StackType_t uxIdleTaskStack[ 2 * configMINIMAL_STACK_SIZE ];

    /* Pass out a pointer to the StaticTask_t structure in which the Idle task’s
    state will be stored. */
    *ppxIdleTaskTCBBuffer = &xIdleTaskTCB;

    /* Pass out the array that will be used as the Idle task’s stack. */
    *ppxIdleTaskStackBuffer = uxIdleTaskStack;

    /* Pass out the size of the array pointed to by *ppxIdleTaskStackBuffer.
    Note that, as the array is necessarily of type StackType_t,
    configMINIMAL_STACK_SIZE is specified in words, not bytes. */
    *pulIdleTaskStackSize = 2 * configMINIMAL_STACK_SIZE;
}

/**
 * A hook that gets called whenever the stack is detected to have been overflown.
 *
 * According to FreeRTOS documentation, it is not guaranteed that this method will detect all overflows.
 */
void vApplicationStackOverflowHook(TaskHandle_t pxCurrentTCB, const char *taskName) {
    resetUART(); // Reset the UART peripheral, in case something went wrong with the memory before
    LOG_EMERGENCY << "Task stack overflow: " << taskName;

    if (pxCurrentTCB != nullptr) {
        // Kill the offending task if it overflows the stack
        vTaskDelete(pxCurrentTCB);
    }
}
}
#pragma clang diagnostic pop