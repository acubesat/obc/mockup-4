#include <ecss-services/inc/Services/ParameterService.hpp>
#include <ecss-services/inc/ServicePool.hpp>
#include <LEDStripTask.h>
#include <MPU9250Task.h>
#include <LEDStripTask.h>
#include <BH1750Task.h>
#include <MCP9808Task.h>
#include <mockup.h>
#include "ECSSObjects.h"


Parameter<uint8_t> redBrightness = Parameter<uint8_t>(0, 0, 3);
Parameter<uint8_t> greenBrightness = Parameter<uint8_t>(0, 0, 3);
Parameter<uint8_t> blueBrightness = Parameter<uint8_t>(0, 0, 255);
Parameter<uint8_t> parameterColourMaster = Parameter<uint8_t>(0, 0, 255);
Parameter<uint32_t> colourPeriod = Parameter<uint32_t>(0, 0, 500);
Parameter<float> colourPhase = Parameter<float>(0, 0, 0.03);

Parameter<float> angleX = Parameter<float>(0,0,0);
Parameter<float> angleY = Parameter<float>(0,0,0);
Parameter<float> angleZ = Parameter<float>(0,0,0);
Parameter<float> gyroX = Parameter<float>(0,0,0);
Parameter<float> gyroY = Parameter<float>(0,0,0);
Parameter<float> gyroZ = Parameter<float>(0,0,0);
Parameter<float> accelX = Parameter<float>(0,0,0);
Parameter<float> accelY = Parameter<float>(0,0,0);
Parameter<float> accelZ = Parameter<float>(0,0,0);

Parameter<float> brightness = Parameter<float>(0,0,0);
Parameter<float> tempInternal = Parameter<float>(0,0,-100);
Parameter<float> tempExternal = Parameter<float>(0,0,-100);

Parameter<uint32_t> taskCount = Parameter<uint32_t>(0,0,0);

std::array<StringParameter, 14> taskNames;
std::array<Parameter<uint32_t>, 14> taskTimes;
std::array<Parameter<uint8_t>, 14> taskStates;

void addECSSObjects() {
    ParameterService & parameters = Services.parameterManagement;

    if (ledStripTask) { // Values should auto-update if the registers change
        colourPeriod.setPtr(static_cast<void *>(&(ledStripTask->updatePeriod)));
        colourPhase.setPtr(static_cast<void *>(&(ledStripTask->phase)));
    }

    if (mpu9250Task) {
        angleX.setPtr((void*)(&(mpu9250Task->calibratedRotation.x)));
        angleY.setPtr((void*)(&(mpu9250Task->calibratedRotation.y)));
        angleZ.setPtr((void*)(&(mpu9250Task->calibratedRotation.z)));
    }

    if (bh1750Task) {
        brightness.setPtr((void*)(&(bh1750Task->brightness)));
    }

    if (mcp9808InternalTask) {
        tempInternal.setPtr((void*)(&(mcp9808InternalTask->temperature)));
    }

    if (mcp9808ExternalTask) {
        tempExternal.setPtr((void*)(&(mcp9808ExternalTask->temperature)));
    }

    parameters.addNewParameter(1, redBrightness);
    parameters.addNewParameter(2, greenBrightness);
    parameters.addNewParameter(3, blueBrightness);
    parameters.addNewParameter(4, parameterColourMaster);
    parameters.addNewParameter(5, colourPeriod);
    parameters.addNewParameter(6, colourPhase);

    parameters.addNewParameter(10, angleX);
    parameters.addNewParameter(11, angleY);
    parameters.addNewParameter(12, angleZ);
    parameters.addNewParameter(13, gyroX);
    parameters.addNewParameter(14, gyroY);
    parameters.addNewParameter(15, gyroZ);
    parameters.addNewParameter(16, accelX);
    parameters.addNewParameter(17, accelY);
    parameters.addNewParameter(18, accelZ);

    parameters.addNewParameter(20, brightness);
    parameters.addNewParameter(21, tempInternal);
    parameters.addNewParameter(22, tempExternal);

    for (int i = 0; i < 14; i++) {
        parameters.addNewParameter(30 + i, taskNames[i]);
        parameters.addNewParameter(50 + i, taskTimes[i]);
        parameters.addNewParameter(70 + i, taskStates[i]);
    }

    parameters.addNewParameter(90, taskCount);

    FunctionManagementService & functions = Services.functionManagement;
    functions.include("led_toggle", led_toggle);
    functions.include("led_strip", led_strip);
    functions.include("led_strip_m", led_strip_m);
    functions.include("led_strip_c", led_strip_c);
    functions.include("led_strip_g", led_strip_g);
    functions.include("led_AUTOfade_on", led_AUTOfade_on);
    functions.include("led_AUTOfade_off", led_AUTOfade_off);
    functions.include("flash_white", flash_white);
    functions.include("flash_fluorescent", flash_fluorescent);
    functions.include("calib_euler", calib_euler);
    functions.include("calib_gyro", calib_gyro);

    HousekeepingService & housekeeping = Services.housekeeping;
//    housekeeping.addHousekeepingStructure(1, {500, {10, 11, 12}});
//    housekeeping.addHousekeepingStructure(2, {60, {20, 21, 22}});
//    housekeeping.addHousekeepingStructure(3, {60, {13, 14, 15}});
    housekeeping.addHousekeepingStructure(4, {50, {10, 11, 12, 20, 21, 22, 13, 14, 15}});
    housekeeping.addHousekeepingStructure(5, {4962, {30,31,32,33,34,35,36}});
    housekeeping.addHousekeepingStructure(6, {4998, {37,38,39,40,41,42,43}});
    housekeeping.addHousekeepingStructure(7, {799, {50,51,52,53,54,55,56,57,58,59,60,61,62,63,70,71,72,73,74,75,76,77,78,79,80,81,82,83,90}});
}

void led_toggle(String<ECSS_FUNCTION_MAX_ARG_LENGTH> a) {
    HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);
}

void led_strip(String<ECSS_FUNCTION_MAX_ARG_LENGTH> a) {
    // Read the colours from the parameters and convert them to a uint8
    static LEDStripTask::RGB temp[LEDStripTask::ledCount];
    for (int i = 0; i < LEDStripTask::ledCount; i++) {
        temp[i].r = redBrightness.getValue();
        temp[i].g = greenBrightness.getValue();
        temp[i].b = blueBrightness.getValue();
    }
    ledStripTask->fadeInto(temp);
}

void led_strip_m(String<ECSS_FUNCTION_MAX_ARG_LENGTH> a) {
    ledStripTask->setCurrentEffect(LEDStripTask::ManualMode);
}

void led_strip_c(String<ECSS_FUNCTION_MAX_ARG_LENGTH> a) {
    ledStripTask->setCurrentEffect(LEDStripTask::ColorMode);
}

void led_strip_g(String<ECSS_FUNCTION_MAX_ARG_LENGTH> a) {
    ledStripTask->setCurrentEffect(LEDStripTask::GradualColorMode);
}

void led_AUTOfade_off(String<ECSS_FUNCTION_MAX_ARG_LENGTH> a) {
    vTaskSuspend(xFadeTaskHandle);
    ledStripTask->fadeDuration = 200;
}

void led_AUTOfade_on(String<ECSS_FUNCTION_MAX_ARG_LENGTH> a) {
    vTaskResume(xFadeTaskHandle);
    ledStripTask->fadeDuration = 1000;
}

void flash_white(String<ECSS_FUNCTION_MAX_ARG_LENGTH> a) {
    ledStripTask->setCurrentEffect(LEDStripTask::ManualMode);
    ledStripTask->parseRGB(coloursO);
    vTaskDelay(pdMS_TO_TICKS(10));
    ledStripTask->parseRGB(colours6);
    vTaskDelay(pdMS_TO_TICKS(4));
    ledStripTask->parseRGB(coloursO);
}

void flash_fluorescent(String<ECSS_FUNCTION_MAX_ARG_LENGTH> a) {
    ledStripTask->setCurrentEffect(LEDStripTask::ManualMode);
    ledStripTask->parseRGB(coloursO);
    vTaskDelay(pdMS_TO_TICKS(20));
    ledStripTask->parseRGB(coloursG);
    vTaskDelay(pdMS_TO_TICKS(8));
    ledStripTask->parseRGB(coloursO);
    vTaskDelay(pdMS_TO_TICKS(200));
    ledStripTask->parseRGB(coloursB);
    vTaskDelay(pdMS_TO_TICKS(8));
    ledStripTask->parseRGB(coloursO);
}

void calib_euler(String<ECSS_FUNCTION_MAX_ARG_LENGTH> a) {
    if (mpu9250Task) {
        mpu9250Task->startAngleCalibration();
    }
}

void calib_gyro(String<ECSS_FUNCTION_MAX_ARG_LENGTH> a) {
    if (mpu9250Task) {
        mpu9250Task->startCalibration();
    }
}

