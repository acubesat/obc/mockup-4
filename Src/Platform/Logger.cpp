#include <Logger.hpp>
#include <UARTTask.h>
#include "cobs-c/cobs.h"
#include "main.h"

void Logger::log(Logger::LogLevel level, etl::istring & message) {
    char uartMessage[254];
    char uartOutput[256];
    char name[10];

    if (level <= Logger::trace) {
        strcpy(name, "trace");
    } else if (level <= Logger::debug) {
        strcpy(name, "debug");
    } else if (level <= Logger::info) {
        strcpy(name, "info");
    } else if (level <= Logger::notice) {
        strcpy(name, "notice");
    } else if (level <= Logger::warning) {
        strcpy(name, "warning");
    } else if (level <= Logger::error) {
        strcpy(name, "error");
    } else {
        strcpy(name, "emergency");
    }

    size_t count = snprintf(uartMessage, 254, "%-7lu [%-7s] %s\n", HAL_GetTick(), name, message.c_str());

    uartTask->queueUARTMessage({uartMessage, static_cast<uint8_t>(count), MessageType::Log});
}