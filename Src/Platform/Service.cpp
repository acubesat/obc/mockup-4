#include "main.h"
#include <iostream>
#include <iomanip>
#include <inc/Logger.hpp>
#include "FreeRTOS.h"
#include <freertos/include/queue.h>
#include <mockup.h>
#include "Service.hpp"

void Service::storeMessage(Message& message) {
	// appends the remaining bits to complete a byte
	message.finalize();

	// Generate a report and log it away
	LOG_DEBUG << "New " << ((message.packetType == Message::TM) ? "TM" : "TC")
	    << " [" << message.serviceType << "," << message.messageType << "]";

	// Send the report via AT86RF233
	if (xQueueSend(xECSSQueueHandle, &message, 0) == pdFALSE) {
	    LOG_WARNING << "ECSS queue is full";
	}
}
