#include <at86rf233/at86rf2xx.hpp>
#include <main.h>
#include <mockup.h>
#include <ecss-services/inc/Logger.hpp>
#include <ecss-services/inc/MessageParser.hpp>
#include <UARTTask.h>
#include "AT86RF233Task.h"
#include "FreeRTOS.h"
#include "task.h"

void AT86RF233Task::at86_receive_data(AT86RF2XX & at) {
    // Get the packet length (first item in FIFO)
    uint16_t pkt_len = at.rx_len();

    Logger::format.decimal();
    LOG_INFO << "Just received " << pkt_len << " bytes of RX data";

    if (pkt_len > 255) {
        LOG_ERROR << "Packet too large";
        // Consume some FIFO bytes
        at.rx_read(receivedData, 255, 0);
        return;
    }

    at.rx_read(receivedData, pkt_len, 0);

    currentMessage = MessageParser::parseECSSTC(receivedData);
    xQueueSend(xTCQueueHandle, &currentMessage, 0);
}


void AT86RF233Task::at86_eventHandler(AT86RF2XX & at) {
    /* If transceiver is sleeping register access is impossible and frames are
     * lost anyway, so return immediately.
     */
    uint8_t state = at.get_status();
    if (state == AT86RF2XX_STATE_SLEEP)
        return;

    /* read (consume) device status */
    uint8_t irq_mask = at.reg_read(AT86RF2XX_REG__IRQ_STATUS);

    /*  Incoming radio frame! */
    if (irq_mask & AT86RF2XX_IRQ_STATUS_MASK__RX_START) {
        // Evt event start
    }

    /*  Done receiving radio frame; call our receive_data function.
     */
    if (irq_mask & AT86RF2XX_IRQ_STATUS_MASK__TRX_END) {
        if (state == AT86RF2XX_STATE_RX_AACK_ON ||
            state == AT86RF2XX_STATE_BUSY_RX_AACK) {
            at86_receive_data(at);
        }
    }
}

void AT86RF233Task::operator()() {
    AT86RF2XX trs = AT86RF2XX(&hspi1);

    xSemaphoreTake(xSPIMutex, portMAX_DELAY);

    trs.init();
    trs.set_chan(26);

    xSemaphoreGive(xSPIMutex);

    while (true) {
        BaseType_t queueResult = xQueueReceive(xECSSQueueHandle, &currentMessage, 50);

        // First, ping the transceiver for any received data

        if (xSemaphoreTake(xSPIMutex, pdMS_TO_TICKS(10)) == pdTRUE) {
            at86_eventHandler(trs);
            xSemaphoreGive(xSPIMutex);
        } else {
            LOG_ERROR << "Could not take RX semaphore for AT86RF233";
        }

        // If no data is in the queue, keep waiting
        if (queueResult == pdFALSE) {
            continue;
        }

        Logger::format.decimal();
        LOG_DEBUG << "TX " << currentMessage.dataSize << "b";

        // Now, the message can be converted into bytes
        composed = MessageParser::compose(currentMessage);

        if (xSemaphoreTake(xSPIMutex, pdMS_TO_TICKS(100)) == pdTRUE) {
            trs.send(reinterpret_cast<uint8_t*>(composed.data()), composed.length());

            // Also send the message via UART
            uartTask->queueUARTMessage({
                composed.data(), static_cast<uint8_t>(composed.length()), MessageType::SpacePacket
            });

            xSemaphoreGive(xSPIMutex);
        } else {
            LOG_ERROR << "Could not take semaphore for AT86RF233";
        }
    }
}
