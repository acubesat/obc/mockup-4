#include <stm32l4xx_ll_usart.h>
#include <stm32l4xx_ll_dma.h>
#include <optional>
#include "FreeRTOS.h"
#include "task.h"
#include "UARTTask.h"

UARTTask::UARTTask() {
    // Allocate the queue
    xUARTQueue = xQueueCreate(QUEUE_SIZE, sizeof(Message));
    
    // Enable the DMA Transmission Complete interrupt
    LL_DMA_EnableIT_TC(DMA, DMA_CHANNEL);
}

void UARTTask::operator()() {
    while (true) {
        // Allocation of the message that will be read from the queue
        Message message;

        if (xQueueReceive(xUARTQueue, &message, portMAX_DELAY)) { // Wait until a message is in the queue
            // Encode the message using COBS
            cobs_encode_result result = message.encode(reinterpret_cast<char*>(buffer), sizeof(buffer));

            // Set the DMA parameters
            LL_DMA_SetDataLength(DMA, DMA_CHANNEL, result.out_len); // Set amount of copied bits for DMA
            LL_DMA_ConfigAddresses(DMA, DMA_CHANNEL, reinterpret_cast<uint32_t>(buffer), // Memory address of the buffer
                                   LL_USART_DMA_GetRegAddr(USART, LL_USART_DMA_REG_DATA_TRANSMIT),
                                   LL_DMA_DIRECTION_MEMORY_TO_PERIPH); // Send message from memory to the USART Data Register

            // Start the DMA transaction
            LL_USART_EnableDMAReq_TX(USART); // Enable DMA in the USART registers
            LL_DMA_EnableChannel(DMA, DMA_CHANNEL); // Enable the DMA transaction

            // Wait until we get an interrupt that the transaction is complete
            xTaskNotifyWait(0x00, 0x00, nullptr, 1000);

            // Prepare for the next UART message
            LL_DMA_DisableChannel(DMA, DMA_CHANNEL);
        }
    }
}

