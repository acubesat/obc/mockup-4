#include <BH1750Task.h>
#include <BH1750.h>
#include "mockup.h"
#include <FreeRTOS.h>
#include <freertos/include/task.h>
#include <ecss-services/inc/Logger.hpp>
#include <freertos/include/semphr.h>

void BH1750Task::operator()() {
    xSemaphoreTake(xI2CMutex, pdMS_TO_TICKS(portMAX_DELAY));
    BH1750_Init(BH1750_CONTHRES);
    xSemaphoreGive(xI2CMutex);
    LOG_INFO << "BH1750 startup done";

    while (true) {
        if (xSemaphoreTake(xI2CMutex, pdMS_TO_TICKS(1000)) == pdFALSE) {
            LOG_ERROR << "Could not get I2C Mutex for BH1750";
        } else {
            brightness = static_cast<float>(BH1750_GetBrightnessCont());
            LOG_TRACE << "Brightness is " << brightness;

            xSemaphoreGive(xI2CMutex);
            vTaskDelay(100);
        }
    }
}