#include <ecss-services/inc/Logger.hpp>
#include <MadgwickAHRS/MadgwickAHRS.h>
#include "MPU9250Task.h"
#include "MPU9250.h"
#include "MPU9250_Definitions.h"
#include "FreeRTOS.h"
#include "task.h"
#include "mockup.h"
#include "ECSSObjects.h"

void MPU9250Task::operator()() {
    xSemaphoreTake(xI2CMutex, pdMS_TO_TICKS(portMAX_DELAY));

    MPU9250Calibration(gyrCal); //Get the gyroscope calibration values
    MPU9250Init(AFS_2G, GFS_500DPS); //Initialize the MPU9250
    MPU9250_SetFullScaleGyroRange(GFS_2000DPS); //Set the gyroscope scale to full scale

    AK8963Init(AK8963_16BIT, AK8963_CONT100HZ, magnAdj);

    xSemaphoreGive(xI2CMutex);
    LOG_INFO << "BH1750 startup done";

    double calibAccel[3] = {0}; // Calibrated accelerometer data
    double calibGyro[3] = {0}; // Calibrated gyroscope data
    float magnField[3] = {0}; // Calibrated magnetic data

    // Start calibrating to get good initial values
    startCalibration();

    TickType_t xLastWakeTime;

    while (true) {
        xLastWakeTime = xTaskGetTickCount();

        if (xSemaphoreTake(xI2CMutex, pdMS_TO_TICKS(1000)) == pdFALSE) {
            LOG_ERROR << "Could not get I2C Mutex for MPU9250";
        } else {
            uint8_t id = AK8963GetID();

            AK8963GetMagnuT(magnField, magnAdj);
            MPU9250_GetCalibAccelGyro(calibAccel, calibGyro, gyrCal);

            if (status == Running) {
                // Calibrate the data
                adjust3Values(calibAccel, calibrationAccelerometer, calibAccel);
                adjust3Values(calibGyro, calibrationGyroscope, calibGyro);

                gyroX.setCurrentValue(calibGyro[0]);
                gyroY.setCurrentValue(calibGyro[1]);
                gyroZ.setCurrentValue(calibGyro[2]);
                accelX.setCurrentValue(calibAccel[0]);
                accelY.setCurrentValue(calibAccel[1]);
                accelZ.setCurrentValue(calibAccel[2]);

                // Throw the data in Magdwick
                MadgwickAHRSupdate(
                        calibGyro[0], calibGyro[1], calibGyro[2],
                        calibAccel[0], calibAccel[1], calibAccel[2],
                        magnField[0], magnField[1], magnField[2]);

                // Convert quaternion to Euler
                uncalibratedRotation = quaternionToEuler(q0, q1, q2, q3);

                // Read the data
                Logger::format.precision(2);

//                LOG_TRACE << "AK8963 field is " << magnField[0] << " " << magnField[1] << " " << magnField[2];
//                LOG_TRACE << "MPU9250 angular velocity is " << calibGyro[0] << " " << calibGyro[1] << " " << calibGyro[2];
//                LOG_TRACE << "MPU9250 linear  accel    is " << calibAccel[0] << " " << calibAccel[1] << " " << calibAccel[2];
//                LOG_TRACE << "CURRENT ROTATION " << uncalibratedRotation.x << "\t" << uncalibratedRotation.y << "\t" << uncalibratedRotation.z;

                // Calibrate the rotation
                calibratedRotation = getCalibratedAngle();
            } else if (status == Calibrating) {

                // Store the data as an average for calibration
                for (int i = 0; i < 3; i++) {
                    calibrationAccelerometer[i] += calibAccel[i] / static_cast<double>(maxCalibrationCount);
                    calibrationGyroscope[i] += calibGyro[i] / static_cast<double>(maxCalibrationCount);
                }

                calibrationCounter--;
                if (calibrationCounter <= 0) {
                    // Calibration complete
                    status = Running;

                    // Reset the MadgwickAHRS values
                    q0 = 1.0f;
                    q1 = 0.0f;
                    q2 = 0.0f;
                    q3 = 0.0f;
                }
            }

            xSemaphoreGive(xI2CMutex);
//            vTaskDelay(pdMS_TO_TICKS(5));
//            TODO: Make this go faster
            vTaskDelayUntil( &xLastWakeTime, pdMS_TO_TICKS(5) );
        }
    }
}
