#include <ECSSObjects.h>
#include <ecss-services/inc/Logger.hpp>
#include "TaskListTask.h"

#include "FreeRTOS.h"
#include "task.h"

void TaskListTask::operator()() {
    while(true) {
        numTasks = uxTaskGetSystemState(statuses, maxTasks, nullptr);

        for (int i = 0; i < numTasks; i++) {
            uint8_t index = statuses[i].xTaskNumber;
            if (index >= maxTasks) {
                LOG_WARNING << "Too large task index";
            }

            taskNames[index].setCurrentValue(statuses[i].pcTaskName);
            taskTimes[index].setCurrentValue(statuses[i].ulRunTimeCounter);
            taskStates[index].setCurrentValue(statuses[i].eCurrentState);
        }
        taskCount.setCurrentValue(numTasks);

        vTaskDelay(pdMS_TO_TICKS(500));
    }
}

extern std::optional<TaskListTask> taskListTask;
