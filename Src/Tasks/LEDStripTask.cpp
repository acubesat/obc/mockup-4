#include <cstring>
#include <ecss-services/inc/Logger.hpp>
#include <ECSSObjects.h>
#include "LEDStripTask.h"

void LEDStripTask::operator()() {
    //TODO: Calculate TIM->CCR2 manually
    LL_DMA_ConfigAddresses(DMA, DMA_CHANNEL, (uint32_t) dmaValues, // Memory address of the buffer
                           (uint32_t) (&(TIM->CCR2)),
                           LL_DMA_DIRECTION_MEMORY_TO_PERIPH); // Send message from memory to the USART Data Register
    LL_DMA_SetDataLength(DMA, DMA_CHANNEL, dataSize); // Set amount of copied bits for DMA

    LL_TIM_EnableDMAReq_CC2(TIM);              /* Enable DMA requests on channel 1 */
    LL_DMA_EnableChannel(DMA, DMA_CHANNEL);
    LL_TIM_CC_EnableChannel(TIM, TIM_CHANNEL);  /* Enable output on channel */
    LL_TIM_EnableCounter(TIM);

    uint8_t toParse[] = {
            255, 0, 0,
            255, 0, 255,
            static_cast<uint8_t>((rand() % 255)), 0, static_cast<uint8_t>((rand() % 255)),
            0, 0, 255,
            0, 0, 255,
            0, 0, 255,
            0, 0, 255,
            0, 255, 255,
    };
    parseRGB(toParse);

    uint8_t lastCount = xTaskGetTickCount();

    while (true) {
        if (currentEffect == ColorMode) {
            for (int i = 0; i < ledCount; i++) {
                hsvStorage[i].h = normalize(sin(2 * M_PI / (double) updatePeriod * xTaskGetTickCount()));
                hsvStorage[i].s = 255;
                hsvStorage[i].v = 255;

                rgbStorage[i] = hsvToRgb(hsvStorage[i]);
            }
            parseRGB(rgbStorage);
        } else if (currentEffect == GradualColorMode) {
            for (int i = 0; i < ledCount; i++) {
                hsvStorage[i].h = normalize(sin(2 * M_PI * (phase * i +  1.0f / (double) updatePeriod * xTaskGetTickCount())));
                hsvStorage[i].s = 255;
                hsvStorage[i].v = 255;

                rgbStorage[i] = hsvToRgb(hsvStorage[i]);
            }
            parseRGB(rgbStorage);
        } else if (currentEffect == FadingMode) {
            for (int i = 0; i < ledCount; i++) {
                // Ratio of change (starts at 0, ends at 1)
                double r = (xTaskGetTickCount() - fadeStart) / (double) fadeDuration;

                if (r > 1) r = 1; // Sanity check, we don't jump integer ticks

                rgbStorage[i].r = (1-r) * rgbStorage2[i].r + r * fadeTarget[i].r;
                rgbStorage[i].g = (1-r) * rgbStorage2[i].g + r * fadeTarget[i].g;
                rgbStorage[i].b = (1-r) * rgbStorage2[i].b + r * fadeTarget[i].b;
            }
            parseRGB(rgbStorage);

            if (xTaskGetTickCount() - fadeStart >= fadeDuration) {
                // Fading complete
                currentEffect = ManualMode;
            }
        }


        if (currentEffect == ManualMode) {
            xTaskNotifyWait(0x00, 0x00, nullptr, pdMS_TO_TICKS(10000));
        } else {
            vTaskDelay(pdMS_TO_TICKS(1));
        }
    }
}

void LEDStripTask::parseRGB(const uint8_t *rgbData) {
    // We assume that the first dmaData registers are left untouched as zero
    for (size_t i = 0; i < ledCount * ledColours; i++) {
        // The index of the current LED in the dmaValues array
        uint32_t index = emptySize + bitDepth * i;
        if (i % ledColours == 0) {
            // Red
            index += bitDepth;
        } else if (i % ledColours == 1) {
            // Green
            index -= bitDepth;
        }

        uint8_t datum = rgbData[i] * (parameterColourMaster.getValue() / 255.0f); // Add the master brightness

        for (uint8_t bit = 0; bit < bitDepth; bit++) {
            // Get the bit'th bit, and store 1 or 2 according to whether it's 0 or 1
            dmaValues[index + bit] = ((datum >> (bitDepth - bit - 1U)) & 0x01U) ? 2 : 1;
        }
    }
}

// Source for RGB-HSV conversion codes:
// https://stackoverflow.com/a/14733008

LEDStripTask::HSV LEDStripTask::rgbToHsv(LEDStripTask::RGB rgb) {
    HSV hsv;
    uint8_t rgbMin, rgbMax;

    rgbMin = rgb.r < rgb.g ? (rgb.r < rgb.b ? rgb.r : rgb.b) : (rgb.g < rgb.b ? rgb.g : rgb.b);
    rgbMax = rgb.r > rgb.g ? (rgb.r > rgb.b ? rgb.r : rgb.b) : (rgb.g > rgb.b ? rgb.g : rgb.b);

    hsv.v = rgbMax;
    if (hsv.v == 0) {
        hsv.h = 0;
        hsv.s = 0;
        return hsv;
    }

    hsv.s = 255 * long(rgbMax - rgbMin) / hsv.v;
    if (hsv.s == 0) {
        hsv.h = 0;
        return hsv;
    }

    if (rgbMax == rgb.r)
        hsv.h = 0 + 43 * (rgb.g - rgb.b) / (rgbMax - rgbMin);
    else if (rgbMax == rgb.g)
        hsv.h = 85 + 43 * (rgb.b - rgb.r) / (rgbMax - rgbMin);
    else
        hsv.h = 171 + 43 * (rgb.r - rgb.g) / (rgbMax - rgbMin);

    return hsv;
}

LEDStripTask::RGB LEDStripTask::hsvToRgb(LEDStripTask::HSV hsv) {
    RGB rgb = {0, 0, 0};
    uint8_t region, remainder, p, q, t;

    if (hsv.s == 0) {
        rgb.r = hsv.v;
        rgb.g = hsv.v;
        rgb.b = hsv.v;
        return rgb;
    }

    region = hsv.h / 43;
    remainder = (hsv.h - (region * 43)) * 6;

    p = (hsv.v * (255 - hsv.s)) >> 8;
    q = (hsv.v * (255 - ((hsv.s * remainder) >> 8))) >> 8;
    t = (hsv.v * (255 - ((hsv.s * (255 - remainder)) >> 8))) >> 8;

    switch (region) {
        case 0:
            rgb.r = hsv.v;
            rgb.g = t;
            rgb.b = p;
            break;
        case 1:
            rgb.r = q;
            rgb.g = hsv.v;
            rgb.b = p;
            break;
        case 2:
            rgb.r = p;
            rgb.g = hsv.v;
            rgb.b = t;
            break;
        case 3:
            rgb.r = p;
            rgb.g = q;
            rgb.b = hsv.v;
            break;
        case 4:
            rgb.r = t;
            rgb.g = p;
            rgb.b = hsv.v;
            break;
        default:
            rgb.r = hsv.v;
            rgb.g = p;
            rgb.b = q;
            break;
    }

    return rgb;
}

void LEDStripTask::parseRGB(const LEDStripTask::RGB *rgbData) {
    // We assume that the first dmaData registers are left untouched as zero
    for (size_t i = 0; i < ledCount; i++) {
        // The index of the current LED in the dmaValues array
        uint32_t index = emptySize + bitDepth * ledColours * i;

        // The adjustment value for the global parameter settings
        float adjust = parameterColourMaster.getValue() / 255.0f;

        RGB datum = {
                static_cast<uint8_t >(rgbData[i].r * adjust),
                static_cast<uint8_t >(rgbData[i].g * adjust),
                static_cast<uint8_t >(rgbData[i].b * adjust),
        };

        for (uint8_t bit = 0; bit < bitDepth; bit++) {
            // Get the bit'th bit, and store 1 or 2 according to whether it's 0 or 1
            dmaValues[index + bitDepth + bit] = ((datum.r >> (bitDepth - bit - 1U)) & 0x01U) ? 2 : 1;
            dmaValues[index + bit] = ((datum.g >> (bitDepth - bit - 1U)) & 0x01U) ? 2 : 1;
            dmaValues[index + 2 * bitDepth + bit] = ((datum.b >> (bitDepth - bit - 1U)) & 0x01U) ? 2 : 1;
        }
    }
}


LEDStripTask::RGB colours1[8] = {
        {255, 0, 0,},
        {255, 0, 0,},
        {255, 0, 0,},
        {255, 0, 0,},
        {255, 0, 0,},
        {255, 0, 0,},
        {255, 0, 0,},
        {255, 0, 0,},
};
LEDStripTask::RGB colours2[] = {
        {0, 255, 0,},
        {0, 255, 0,},
        {0, 255, 0,},
        {0, 255, 0,},
        {0, 255, 0,},
        {0, 255, 0,},
        {0, 255, 0,},
        {0, 255, 0,},
};
LEDStripTask::RGB colours3[] = {
        {255, 255, 0,},
        {0,   255, 255,},
        {0,   255, 0,},
        {255, 50,  0,},
        {0,   0,   0,},
        {0,   0,   0,},
        {0,   0,   0,},
        {1,   1,   1,},
};
LEDStripTask::RGB colours4[] = {
        {0, 0, 255,},
        {16,   0, 255,},
        {32,   0, 255,},
        {64, 0,  255,},
        {96,   0,   255,},
        {128,   0,   255,},
        {156,   0,   255,},
        {255,   1,   255,},
};
LEDStripTask::RGB coloursO[] = {
        {0, 0, 0,},
        {0,   0, 0,},
        {0,   0, 0,},
        {0, 0,  0,},
        {0,   0,   0,},
        {0,   0,   0,},
        {0,   0,   0,},
        {0,   0,   0,},
};
LEDStripTask::RGB colours6[] = {
        {254, 254, 254,},
        {254,   254, 254,},
        {254,   254, 254,},
        {254, 254,  254,},
        {254,   254,   254,},
        {254,   254,   254,},
        {254,   254,   254,},
        {254,   254,   254,},
};
LEDStripTask::RGB coloursR[] = {
        {254, 0, 0,},
        {254,   0, 0,},
        {254,   0, 0,},
        {254, 0,  0,},
        {254,   0,   0,},
        {254,   0,   0,},
        {254,   0,   0,},
        {254,   0,   0,},
};

LEDStripTask::RGB coloursG[] = {
        {0, 254,0},
        {0, 254,0},
        {0, 254,0},
        {0, 254,0},
        {0, 254,0},
        {0, 254,0},
        {0, 254,0},
        {0, 254,0},
};

LEDStripTask::RGB coloursB[] = {
        {0, 0,254},
        {0, 0,254},
        {0, 0,254},
        {0, 0,254},
        {0, 0,254},
        {0, 0,254},
        {0, 0,254},
        {0, 0,254},
};
