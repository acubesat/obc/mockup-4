#include <Logger.hpp>
#include <mockup.h>
#include "FreeRTOS.h"
#include "task.h"
#include "MCP9808Task.h"

void MCP9808Task::operator()() {
    while(true) {
        if (xSemaphoreTake(xI2CMutex, pdMS_TO_TICKS(1000)) == pdFALSE) {
            LOG_ERROR << "Could not get I2C Mutex for MCP9808";
        } else {
            float tempTemperature = 0.0f;
            mcp9808.getTemp(tempTemperature);
            if (temperature == 0.0f) {
                LOG_WARNING << "Could not get reading of temperature";
            } else {
                temperature = tempTemperature;
            }

            Logger::format.decimal().precision(1);
            LOG_TRACE << "Temperature: " << temperature;

            xSemaphoreGive(xI2CMutex);
            vTaskDelay(pdMS_TO_TICKS(150));
        }
    }
}
