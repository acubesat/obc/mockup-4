#include "FreeRTOS.h"
#include <freertos/include/task.h>
#include <ecss-services/inc/ServicePool.hpp>
#include <ecss-services/inc/Logger.hpp>
#include <freertos/include/queue.h>
#include <mockup.h>
#include "ECSSPeriodicTask.h"

void ECSSPeriodicTask::operator()() {
    while (true) {
        if (xQueueReceive(xTCQueueHandle, &currentTC, 5) == pdTRUE) {
            Logger::format.decimal();
            LOG_INFO << "Executing TC[" << currentTC.serviceType << "," << currentTC.messageType << "]";
            MessageParser::execute(currentTC);
        }
        // Housekeeping
        Services.housekeeping.checkAndSendHousekeepingReports((xTaskGetTickCount()));
    }
}
