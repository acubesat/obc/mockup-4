#include <main.h>
#include "TWI.h"

/***************************************************
 * Important!!!
 * Generally you should read SR1 first and then SR2
 ***************************************************/

void TWIInit(void)
{
    // Init is done by HAL
}
//
//void TWIWrite(uint8_t u8data)
//{
//    I2C1->TXDR = u8data; //Write data to the register
//    while(!(I2C1->ISR & I2C_SR1_BTF) || !(I2C1->SR2 & I2C_SR2_BUSY)); //Wait until the byte transfer is complete
//}
//
//void TWIStart(void)
//{
//    I2C1->CR1 |= I2C_CR1_START; //Generate a start sequence
//    while(!(I2C1->SR1 &I2C_SR1_SB) || !(I2C1->SR2 & I2C_SR2_BUSY)); //Wait until a start sequence is generated
//}
//
//void TWIStop(void)
//{
//    I2C1->CR1 |= I2C_CR1_STOP; //Generate a stop bit
//}
//
//uint8_t TWIReadACK(void)
//{
//    I2C1->CR1 |= I2C_CR1_ACK; //Enable acknowledge
//    while(!(I2C1->SR1 & I2C_SR1_RXNE) || !(I2C1->SR2 & I2C_SR2_BUSY)); //Wait until data is received
//    return I2C1->DR; //Read the data from the register
//}
//
//uint8_t TWIReadNACK(void)
//{
//    I2C1->CR1 &= ~I2C_CR1_ACK; //Disable acknowledge
//    while(!(I2C1->SR1 & I2C_SR1_RXNE) || !(I2C1->SR2 & I2C_SR2_BUSY)); //Wait until data is received
//    return I2C1->DR; //Read the data from the register
//}
//
////You need to call that function after generating a start sequence to address the device
//void TWISendAddr(uint8_t addr, uint8_t tr_dir)
//{
//    if(tr_dir) //Set to direction as transmitter, which means we are sending a command
//    {
//        I2C1->DR = ((addr << 1)&0xFE); //Set the address
//        uint16_t sr1_mask = I2C_SR1_ADDR|I2C_SR1_TXE;
//        uint16_t sr2_mask = I2C_SR2_BUSY|I2C_SR2_TRA|I2C_SR2_MSL;
//        while(!(I2C1->SR1 & sr1_mask) || !(I2C1->SR2 & sr2_mask));
//    }
//    else //Direction receiver, or we want to receive data
//    {
//        I2C1->DR = ((addr << 1)|0x01);
//        while(!(I2C1->SR1 & I2C_SR1_ADDR) || !(I2C1->SR2 & (I2C_SR2_BUSY|I2C_SR2_MSL)));
//    }
//}
//
//uint8_t TWIReadByte(uint8_t dev_addr, uint8_t registe)
//{
//    uint8_t byte_read = 0; //Save the read byte
//
//    TWIStart(); //Send a start condition to initialize communication
//    TWISendAddr(dev_addr, 1); //Send the write command to the sensor memory
//    TWIWrite(registe); //Send the register you want
//
//    //TWI Restart condition
//    TWIStart();
//
//    TWISendAddr(dev_addr, 0); //Send the read command to the sensor
//    // TODO: Do we need to send the stop bit earlier?
//    TWIStop(); //Signal the microcontroller to stop I2C communication after the next byte is sent
//    byte_read = TWIReadNACK(); //Read one byte
//
//    return byte_read;
//}
//
//void TWIReadBytes(uint8_t dev_addr, uint8_t registe, uint8_t *byte_read, uint8_t byte_count)
//{
//    TWIStart(); //Send a start condition to initialize communication
//    TWISendAddr(dev_addr, 1); //Send the write command to the sensor memory
//    TWIWrite(registe); //Send the register you want
//
//    //TWI Restart condition
//    TWIStart();
//
//    TWISendAddr(dev_addr, 0); //Send the read command to the sensor
//    for(uint8_t i = 0; i < byte_count; i++) //and read the returned bytes
//    {
//        if (i == byte_count - 1) {
//            TWIStop(); //Signal the microcontroller to stop I2C communication after the next byte is sent
//            byte_read[i] = TWIReadNACK(); //When the last byte is to be received, don't acknowledge to the slave
//        } else {byte_read[i] = TWIReadACK();} //If the bytes that are expected are more than one, send an acknowledgment to the slave
//    }
//}
//

void TWIReadBytes(uint8_t dev_addr, uint8_t registe, uint8_t *byte_read, uint8_t byte_count)
{
    HAL_I2C_Master_Transmit(&hi2c1, dev_addr, &registe, 1, 100);
    HAL_I2C_Master_Receive(&hi2c1, dev_addr, byte_read, byte_count, 200);
}

uint8_t TWIReadByte(uint8_t dev_addr, uint8_t registe)
{
    uint8_t rxData = 0;

    HAL_I2C_Master_Transmit(&hi2c1, dev_addr, &registe, 1, 100);
    HAL_I2C_Master_Receive(&hi2c1, dev_addr, &rxData, 1, 100);

    return rxData;
}

void TWIWriteByte(uint8_t dev_addr, uint8_t command_register, uint8_t command)
{
    uint8_t txData[2];  // data to be transmitted
    txData[0] = command_register;   // register address
    txData[1] = command; // data

    HAL_I2C_Master_Transmit(&hi2c1, dev_addr, txData, 2, 100);
}
