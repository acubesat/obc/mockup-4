#ifndef MOCKUP_4_UARTTASK_H
#define MOCKUP_4_UARTTASK_H

#include <FreeRTOS.h>
#include <freertos/include/queue.h>
#include <cstring>
#include <cobs-c/cobs.h>
#include "stm32l4xx.h"
#include "stm32l4xx_ll_dma.h"
#include "Task.h"
#include "mockup.h"

enum class MessageType {
    Log = 1, // A log string
    SpacePacket = 2, // A CCSDS space packet
    Ping = 3, // A heartbeat ping
};

/**
 * Task for communicating with the UART peripheral. This is the only class responsible for this, so no conflicts will
 * occur.
 */
class UARTTask : public Task {
private:
    /**
     * Size of the queue for UART messages
     */
    static const size_t QUEUE_SIZE = 10;

    USART_TypeDef* USART = USART1;
    DMA_TypeDef* DMA = DMA2;
    uint32_t DMA_CHANNEL = LL_DMA_CHANNEL_1;

    /**
     * The queue for UART messages
     */
    QueueHandle_t xUARTQueue;

    /**
     * An internal buffer used to store UART messages to be sent through DMA
     */
    uint8_t buffer[257];

    /**
     * A class representing one UART Message. To be used internally to pass information such as length, type etc.
     */
    class Message {
        char message[256]; ///< The message itself, with extra bytes for the extra information

        uint8_t length; ///< Length of the raw UART message in bytes
        MessageType messageType;

        friend UARTTask;
    public:
        Message() = default;

        Message(const char *message, uint8_t length, MessageType messageType) : length(length), messageType(messageType) {
            if (length > 254) length = 254; // Sanity check, since the Message can't contain more than 255 bytes, given COBS encoding

            memcpy(this->message + 1, message, length);
        };

        /**
         * Use COBS (Consistent-Overhead Byte Stuffing) to packetize this message
         * @param output The output character buffer
         * @param outputSize The (maximum) size of the buffer, in bytes
         * @return The cobs encode result. Read the status and the encoded length from it.
         */
        cobs_encode_result encode(char* output, size_t outputSize) {
            // Append the message type to the message itself
            message[0] = static_cast<uint8_t>(messageType);

            cobs_encode_result result = cobs_encode(output, outputSize - 1, message, length + 1);

            // Append a null byte to the end of the result, as dictated by COBS
            output[result.out_len] = '\0';
            result.out_len += 1; // Increase the output size to include this null byte

            return result;
        }
    };

    Message errorMessagePlaceholder{"ERROR_QUEUE_FULL", 16, MessageType::Log};
public:
    /**
     * Pointer to the FreeRTOS task
     * Note: Check for NULL before using!
     */
    xTaskHandle taskHandle;

    /**
     * Constructor
     */
    explicit UARTTask();

    /**
     * Add a UART Message to the queue
     * @param message The structure of a UART message
     */
    inline void queueUARTMessage(const Message & message) {
        // Note that the Message here is queued by copy, and not by reference, as specified in the documentation of
        // xQueueSend
        if (xQueueSend(xUARTQueue, &message, 0) == pdFALSE) {
            xQueueSend(xUARTQueue, &errorMessagePlaceholder, 100);
        };
    }

    void operator()() override;
};

extern std::optional<UARTTask> uartTask;

#endif //MOCKUP_4_UARTTASK_H
