#ifndef MOCKUP_4_TASKLISTTASK_H
#define MOCKUP_4_TASKLISTTASK_H

#include "FreeRTOS.h"
#include <freertos/include/task.h>
#include "Task.h"

class TaskListTask : public Task {
    static constexpr int maxTasks = 14;

    TaskStatus_t statuses[maxTasks];
    UBaseType_t numTasks;

public:
    void operator()() override;
};


#endif //MOCKUP_4_TASKLISTTASK_H
