#ifndef MOCKUP_4_MOCKUP_H
#define MOCKUP_4_MOCKUP_H

#include "FreeRTOS.h"
#include <freertos/include/semphr.h>
#include <optional>

extern SemaphoreHandle_t xI2CMutex;
extern SemaphoreHandle_t xSPIMutex;

extern QueueHandle_t xECSSQueueHandle;
extern QueueHandle_t xTCQueueHandle;

extern TaskHandle_t xFadeTaskHandle;

#endif //MOCKUP_4_MOCKUP_H
