#ifndef MOCKUP_4_TASK_H
#define MOCKUP_4_TASK_H

/**
 * A generic FreeRTOS Task. This is an abstract class
 */
class Task {
public:
    virtual void operator()() {};
};

#endif //MOCKUP_4_TASK_H
