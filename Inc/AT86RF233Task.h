#ifndef MOCKUP_4_AT86RF233TASK_H
#define MOCKUP_4_AT86RF233TASK_H

#include <ecss-services/inc/Message.hpp>
#include "Task.h"
#include <at86rf233/at86rf2xx.hpp>

class AT86RF233Task : public Task {
private:
    Message currentMessage;
    String<CCSDS_MAX_MESSAGE_SIZE> composed = "";

    uint8_t receivedData[255];

    void at86_receive_data(AT86RF2XX & at);
    void at86_eventHandler(AT86RF2XX & at);
public:
    void operator()() override;
};

extern std::optional<AT86RF233Task> at86rf233Task;

#endif //MOCKUP_4_MCP9808TASK_H
