#ifndef MOCKUP_4_MPU9250TASK_H
#define MOCKUP_4_MPU9250TASK_H

#include "Task.h"
#include "ecss-services/inc/Logger.hpp"
#include <cmath>

class MPU9250Task : public Task {
private:
    /**
     * Number of values to get for calibration
     */
    static constexpr int maxCalibrationCount = 50;

    /**
     * The current calibration counter, from \ref calibrationCount to 0
     */
    int calibrationCounter = 10;

    /**
     * Gyroscope calibration
     */
    float gyrCal[3];

    /**
     * Magnetometer adjust
     */
    float magnAdj[3];

    /**
     * Our gyroscope calibration data
     */
    double calibrationGyroscope[3] = {0};

    /**
     * Our accelerometer calibration data
     */
    double calibrationAccelerometer[3] = {0};

    /**
     * Add the calibration values to an array of 3 x-y-z values
     * @tparam T Float or double
     * @param input The original un-calibrated data
     * @param calibration The calibration vectors
     * @param output The calibrated output
     */
    template<typename T>
    inline void adjust3Values(T* input, T* calibration, T* output) {
        output[0] = input[0] - calibration[0];
        output[1] = input[1] - calibration[1];
        output[2] = input[2] - calibration[2];
    }

    enum Status {
        Running,
        Calibrating
    } status = Running;

    struct EulerAngle {float x; float y; float z;};

    /**
     * The Euler angle calibration value (degrees)
     */
    EulerAngle eulerCalibration = {0, 0, 0};

    /**
     * The current uncalibrated rotation of the object (degrees)
     */
    EulerAngle uncalibratedRotation = {0, 0, 0};

    /**
     * Convert a quaternion to Euler angles
     * @param qw q0
     * @param qx q1
     * @param qy q2
     * @param qz q3
     * @return The Euler angle in degrees
     */
    EulerAngle quaternionToEuler(double qw, double qx, double qy, double qz) {
        EulerAngle angle;

        // roll (x-axis rotation)
        double sinr_cosp = +2.0 * (qw * qx + qy * qz);
        double cosr_cosp = +1.0 - 2.0 * (qx * qx + qy * qy);
        angle.x = atan2(sinr_cosp, cosr_cosp);

        // pitch (y-axis rotation)
        double sinp = +2.0 * (qw * qy - qz * qx);
        if (fabs(sinp) >= 1)
            angle.y = copysign(M_PI / 2, sinp); // use 90 degrees if out of range
        else
            angle.y = asin(sinp);

        // yaw (z-axis rotation)
        double siny_cosp = +2.0 * (qw * qz + qx * qy);
        double cosy_cosp = +1.0 - 2.0 * (qy * qy + qz * qz);
        angle.z = atan2(siny_cosp, cosy_cosp);

        // Convert from radians to degrees
        angle.x *= 360.0 / 2 / M_PI;
        angle.y *= 360.0 / 2 / M_PI;
        angle.z *= 360.0 / 2 / M_PI;

        return angle;
    }
public:
    void operator()() override;

    /**
     * The calibrated rotation of the object (degrees)
     */
    EulerAngle calibratedRotation = {0, 0, 0};

    /**
     * Signal the task that it needs to reject the following values in order to start calibrating
     */
    void startCalibration()
    {
        LOG_NOTICE << "Starting calibration";
        status = Calibrating;
        calibrationCounter = maxCalibrationCount;

        // Reset the calibration register
        for (int i = 0; i < 3; i++) {
            calibrationGyroscope[i] = 0;
            calibrationAccelerometer[i] = 0;
        }
    }

    /**
     * Signal the task that it needs to start calibration of the final angle return value
     */
    void startAngleCalibration()
    {
        LOG_NOTICE << "Calibrating Euler angle";

        eulerCalibration = uncalibratedRotation;
    }

    /**
     * Get the calibrated Euler angle
     * @return The Euler angle, in degrees
     */
    EulerAngle getCalibratedAngle() {
        EulerAngle temp = uncalibratedRotation;

        temp.x -= eulerCalibration.x;
        temp.y -= eulerCalibration.y;
        temp.z -= eulerCalibration.z;

        return temp;
    }
};

extern std::optional<MPU9250Task> mpu9250Task;

#endif //MOCKUP_4_MCP9808TASK_H
