#ifndef MOCKUP_4_LEDSTRIPTASK_H
#define MOCKUP_4_LEDSTRIPTASK_H

#include <freertos/include/FreeRTOS.h>
#include "stm32l4xx.h"
#include "stm32l4xx_ll_dma.h"
#include "stm32l4xx_ll_tim.h"
#include "task.h"
#include "Task.h"
#include <cstring>

class LEDStripTask : public Task {
public:
    enum Effect : uint8_t {
        ManualMode = 0, ///< Do not update the LEDs; expect manual updates only, using the parseRGB method
        ColorMode = 1, ///< Change colours of the entire LED bar
        GradualColorMode = 2, ///< Change colours of each LED
        FadingMode = 3, ///< Currently performing a fade
    };

    struct RGB { uint8_t r; uint8_t g; uint8_t b; }; ///< An RGB color
    struct HSV { uint8_t h; uint8_t s; uint8_t v; }; ///< An HSV color

    static constexpr uint16_t ledCount = 8;
    static constexpr uint8_t ledColours = 3;
    static constexpr uint8_t bitDepth = 8;
private:
    DMA_TypeDef* DMA = DMA1;
    uint32_t DMA_CHANNEL = LL_DMA_CHANNEL_1;
    TIM_TypeDef* TIM = TIM2;
    uint32_t TIM_CHANNEL = LL_TIM_CHANNEL_CH2;

    static constexpr uint32_t emptySize = 49; ///< Number of LOW pulses for reset
    static constexpr uint32_t dataSize = ledColours * ledCount * bitDepth + emptySize;

    /**
     * The array containing the values that will be directly fed to the DMA
     */
    volatile uint8_t dmaValues[dataSize] = { 0 };

    /**
     * Stores the current effect of the LED Strip
     */
    Effect currentEffect = GradualColorMode;

    /**
     * A variable that counts when a fade started
     */
    uint32_t fadeStart = 0;

    /**
     * A storage for temporary RGB data
     */
    RGB rgbStorage[ledCount] = { 0 };

    /**
     * A second storage for temporary RGB data
     */
    RGB rgbStorage2[ledCount] = { 0 };

    /**
     * A storage for temporary HSV data
     */
    HSV hsvStorage[ledCount] = { 0 };

    /**
     * A storage for the fade target of the LEDs
     */
    RGB fadeTarget[ledCount] = { 0 };

    HSV rgbToHsv(RGB in); ///< Converts an RGB struct to HSV
    RGB hsvToRgb(HSV hsv); ///< Converts an HSV struct to RGB

    /**
     * Map a -1 to +1 double value to 0-255
     * @param value A value in [-1,+1]
     * @return A value in [0,255]
     */
    constexpr static uint8_t normalize(double value) {
        return 255 * ((value + 1.0f) / 2.0f);
    }
public:
    void operator()() override;

    /**
     * Pointer to the FreeRTOS task
     * Note: Check for NULL before using!
     */
    xTaskHandle taskHandle = nullptr;

    /**
    * Parse RGB data and store them in the DMA values array
    *
    * @param rgbData An array of RGB data, containing a total of ledCount * ledColours 8-bit values, in a format of
    * R1-G1-B1, R2-G2-B2, etc.
    */
    void parseRGB(const uint8_t * rgbData);

    /**
    * Parse RGB data and store them in the DMA values array
    *
    * @param rgbData An array of RGB data, containing a total of ledCount values
    */
    void parseRGB(const RGB * rgbData);

    /**
     * Duration of the fade effect in ticks
     */
    uint32_t fadeDuration = 1000;

    /**
     * Update period for effects in ticks
     */
    uint32_t updatePeriod = 5000;

    /**
     * The phase difference between elements.
     * @details This is a value between 0 and 1, with 1 being equivalent to a phase change of 2*PI, i.e 360 degrees.
     * Use small values (<=0.1) for smooth changes.
     */
    float phase = 0.03;

    /**
     * Start fading into a colour
     * @param rgbData An array of RGB data, containing a total of ledCount values
     */
    void fadeInto(const RGB * rgbData) {
        memcpy(fadeTarget, rgbData, ledCount * sizeof(RGB));
        memcpy(rgbStorage2, rgbStorage, ledCount * sizeof(RGB));
        this->currentEffect = FadingMode;
        fadeStart = xTaskGetTickCount();

        if (taskHandle != nullptr) xTaskNotify(taskHandle, 0, eNoAction);
    }

    /**
     * Sets the effect, effective immediatelly
     * @param currentEffect The effect of the LED stri
     */
    void setCurrentEffect(Effect currentEffect) {
        if (currentEffect == FadingMode) {
            this->currentEffect = ManualMode;
        } else {
            this->currentEffect = currentEffect;
        }

        if (taskHandle != nullptr) xTaskNotify(taskHandle, 0, eNoAction);
    }
};

extern std::optional<LEDStripTask> ledStripTask;

// Some interesting colour definitions

extern LEDStripTask::RGB colours1[8];
extern LEDStripTask::RGB colours2[8];
extern LEDStripTask::RGB colours3[8];
extern LEDStripTask::RGB colours4[8];
extern LEDStripTask::RGB coloursO[8];
extern LEDStripTask::RGB colours6[8];
extern LEDStripTask::RGB coloursR[8];
extern LEDStripTask::RGB coloursG[8];
extern LEDStripTask::RGB coloursB[8];

#endif //MOCKUP_4_LEDSTRIPTASK_H
