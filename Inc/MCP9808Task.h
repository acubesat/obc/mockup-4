#ifndef MOCKUP_4_MCP9808TASK_H
#define MOCKUP_4_MCP9808TASK_H

#include "stm32-component-drivers/MCP9808/Inc/MCP9808.hpp"
#include "Task.h"

class MCP9808Task : public Task {
private:
    I2C_HandleTypeDef* i2c;

    // User defined I2C address (bits A3-A1, see datasheet for acceptable values)
    uint8_t i2cUserAddress = 0x00;

    MCP9808 mcp9808;
public:
    MCP9808Task(I2C_HandleTypeDef *i2c, uint8_t i2cUserAddress)
        : i2c(i2c), i2cUserAddress(i2cUserAddress), mcp9808(i2c, i2cUserAddress) {
    };

    void operator()() override;

    float temperature = -100;
};

extern std::optional<MCP9808Task> mcp9808InternalTask;
extern std::optional<MCP9808Task> mcp9808ExternalTask;

#endif //MOCKUP_4_MCP9808TASK_H
