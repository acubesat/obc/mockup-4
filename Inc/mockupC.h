
#ifndef MOCKUP_4_MOCKUPC_H
#define MOCKUP_4_MOCKUPC_H

/**
 * The main mockup code that will be executed.
 *
 * This function essentially provides a bridge between the STM32CubeMX-generated main.cpp, and the C++-based
 * ecss-services code
 */
void main_cpp();

#endif //MOCKUP_4_MOCKUP_H
