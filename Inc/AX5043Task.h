#ifndef MOCKUP_4_AX5043TASK_H
#define MOCKUP_4_AX5043TASK_H

#include <ecss-services/inc/Message.hpp>
#include "Task.h"

class AX5043Task : public Task {
private:
public:
    void operator()() override;
};

extern std::optional<AX5043Task> ax5043Task;

#endif //MOCKUP_4_MCP9808TASK_H
