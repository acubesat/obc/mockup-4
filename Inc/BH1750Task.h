#ifndef MOCKUP_4_BH1750TASK_H
#define MOCKUP_4_BH1750TASK_H


#include <optional>
#include "Task.h"

class BH1750Task : public Task {
private:
public:
    float brightness = 0;
    void operator()() override;
};

extern std::optional<BH1750Task> bh1750Task;

#endif //MOCKUP_4_MCP9808TASK_H
