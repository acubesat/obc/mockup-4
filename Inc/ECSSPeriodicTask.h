#ifndef MOCKUP_4_ECSSPERIODICTASK_H
#define MOCKUP_4_ECSSPERIODICTASK_H

#include "Task.h"

/**
 * A class for periodic ECSS jobs, such as housekeeping or scheduling
 */
class ECSSPeriodicTask : public Task {
private:
    /**
     * The TC message currently being parsed
     */
    Message currentTC;
public:
    void operator()() override;
};

extern std::optional<ECSSPeriodicTask> ecssPeriodicTask;

#endif //MOCKUP_4_ECSSPERIODICTASK_H
