EESchema Schematic File Version 4
LIBS:Satellite-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	1550 1200 1900 1200
Text Label 1900 1200 0    50   ~ 0
VCC
Text Label 1900 1300 0    50   ~ 0
VBAT
$Comp
L Connector:Conn_01x04_Male J3
U 1 1 5D712479
P 2900 1200
F 0 "J3" H 3008 1481 50  0000 C CNN
F 1 "Conn_01x04_Male" H 3008 1390 50  0000 C CNN
F 2 "" H 2900 1200 50  0001 C CNN
F 3 "~" H 2900 1200 50  0001 C CNN
	1    2900 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 1100 3500 1100
Wire Wire Line
	3100 1300 3500 1300
Wire Wire Line
	3100 1400 3500 1400
Text Label 3500 1100 0    50   ~ 0
GPIO1
Text Label 3500 1200 0    50   ~ 0
GPIO2
Text Label 3500 1300 0    50   ~ 0
GPIO3
Text Label 3500 1400 0    50   ~ 0
GPIO4
$Comp
L Connector:Conn_01x02_Male J2
U 1 1 5D713153
P 1350 1700
F 0 "J2" H 1458 1881 50  0000 C CNN
F 1 "Conn_01x02_Male" H 1458 1790 50  0000 C CNN
F 2 "" H 1350 1700 50  0001 C CNN
F 3 "~" H 1350 1700 50  0001 C CNN
	1    1350 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 1700 1900 1700
Wire Wire Line
	1550 1800 1900 1800
Text Label 1900 1700 0    50   ~ 0
I2C_SDA
Text Label 1900 1800 0    50   ~ 0
I2C_SCL
$Comp
L Connector:Conn_01x03_Male J4
U 1 1 5D713A85
P 2900 1800
F 0 "J4" H 3008 2081 50  0000 C CNN
F 1 "Conn_01x03_Male" H 3008 1990 50  0000 C CNN
F 2 "" H 2900 1800 50  0001 C CNN
F 3 "~" H 2900 1800 50  0001 C CNN
	1    2900 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 1700 3500 1700
Wire Wire Line
	3100 1800 3500 1800
Wire Wire Line
	3100 1900 3500 1900
Text Label 3500 1700 0    50   ~ 0
SCK
Text Label 3500 1800 0    50   ~ 0
MOSI
Text Label 3500 1900 0    50   ~ 0
MISO
$Comp
L Device:LED D1
U 1 1 5D714962
P 5850 2450
F 0 "D1" V 5889 2333 50  0000 R CNN
F 1 "LED" V 5798 2333 50  0000 R CNN
F 2 "" H 5850 2450 50  0001 C CNN
F 3 "~" H 5850 2450 50  0001 C CNN
	1    5850 2450
	0    -1   -1   0   
$EndComp
Text Notes 1100 900  0    50   ~ 0
The power suppy component is green\n\n
Wire Wire Line
	5850 2300 5850 2200
Text Label 5850 2200 0    50   ~ 0
VCC
$Comp
L power:GND #PWR01
U 1 1 5D719326
P 1550 1100
F 0 "#PWR01" H 1550 850 50  0001 C CNN
F 1 "GND" V 1555 972 50  0000 R CNN
F 2 "" H 1550 1100 50  0001 C CNN
F 3 "" H 1550 1100 50  0001 C CNN
	1    1550 1100
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5D71B2F2
P 5850 2600
F 0 "#PWR02" H 5850 2350 50  0001 C CNN
F 1 "GND" H 5855 2427 50  0000 C CNN
F 2 "" H 5850 2600 50  0001 C CNN
F 3 "" H 5850 2600 50  0001 C CNN
	1    5850 2600
	1    0    0    -1  
$EndComp
$Comp
L Led_Strip:Led_Strip U1
U 1 1 5D71F89F
P 5150 1100
F 0 "U1" H 5117 1325 50  0000 C CNN
F 1 "Led_Strip" H 5117 1234 50  0000 C CNN
F 2 "" H 5150 1200 50  0001 C CNN
F 3 "" H 5150 1200 50  0001 C CNN
	1    5150 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 1100 4450 1100
Wire Wire Line
	4650 1300 4450 1300
Text Label 4450 1100 2    50   ~ 0
VCC
$Comp
L power:GND #PWR03
U 1 1 5D72057E
P 4450 1300
F 0 "#PWR03" H 4450 1050 50  0001 C CNN
F 1 "GND" V 4455 1172 50  0000 R CNN
F 2 "" H 4450 1300 50  0001 C CNN
F 3 "" H 4450 1300 50  0001 C CNN
	1    4450 1300
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x04_Male J1
U 1 1 5D7214C7
P 1350 1200
F 0 "J1" H 1458 1481 50  0000 C CNN
F 1 "Conn_01x04_Male" H 1458 1390 50  0000 C CNN
F 2 "" H 1350 1200 50  0001 C CNN
F 3 "~" H 1350 1200 50  0001 C CNN
	1    1350 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 1300 1900 1300
Wire Wire Line
	1550 1400 1900 1400
Text Label 1900 1400 0    50   ~ 0
VAUX
Text Notes 2700 2150 0    50   ~ 0
Right angle connector for the BH1750\n
$Comp
L Connector:Conn_01x05_Female J6
U 1 1 5D72C236
P 2800 2600
F 0 "J6" H 2828 2626 50  0000 L CNN
F 1 "Conn_01x05_Female" H 2828 2535 50  0000 L CNN
F 2 "" H 2800 2600 50  0001 C CNN
F 3 "~" H 2800 2600 50  0001 C CNN
	1    2800 2600
	-1   0    0    1   
$EndComp
Wire Wire Line
	3000 2600 3400 2600
Wire Wire Line
	3000 2500 3400 2500
Wire Wire Line
	3000 2400 3400 2400
$Comp
L power:GND #PWR07
U 1 1 5D7530FD
P 3000 2800
F 0 "#PWR07" H 3000 2550 50  0001 C CNN
F 1 "GND" V 3005 2672 50  0000 R CNN
F 2 "" H 3000 2800 50  0001 C CNN
F 3 "" H 3000 2800 50  0001 C CNN
	1    3000 2800
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5D753BC1
P 3000 2700
F 0 "#PWR06" H 3000 2450 50  0001 C CNN
F 1 "GND" V 3005 2572 50  0000 R CNN
F 2 "" H 3000 2700 50  0001 C CNN
F 3 "" H 3000 2700 50  0001 C CNN
	1    3000 2700
	0    -1   -1   0   
$EndComp
Text Label 3400 2600 0    50   ~ 0
I2C_SDA
Text Label 3400 2500 0    50   ~ 0
I2C_SCL
Text Label 3400 2400 0    50   ~ 0
VCC
Text Notes 3900 2600 0    50   ~ 0
OBC board I2C connector:\n1 -> SCL\n2 -> SDA\n
Wire Notes Line
	1950 2150 4950 2150
Wire Notes Line
	4950 2150 4950 3000
Wire Notes Line
	4950 3000 1950 3000
Wire Notes Line
	1950 3000 1950 2150
Wire Notes Line
	1050 700  1050 1450
Wire Notes Line
	1050 1450 2600 1450
Wire Notes Line
	2600 1450 2600 700 
Wire Notes Line
	2600 700  1050 700 
Wire Wire Line
	4650 1200 4450 1200
Wire Wire Line
	3100 1200 3500 1200
Text Label 4450 1200 2    50   ~ 0
GPIO2
$Comp
L MCP9808:MCP9808T-E_MS IC1
U 1 1 5D7801AF
P 2950 3850
F 0 "IC1" H 3500 4115 50  0000 C CNN
F 1 "MCP9808T-E_MS" H 3500 4024 50  0000 C CNN
F 2 "SOP65P490X110-8N" H 3900 3950 50  0001 L CNN
F 3 "https://ww1.microchip.com/downloads/en/DeviceDoc/25095A.pdf" H 3900 3850 50  0001 L CNN
F 4 "Microchip MCP9808T-E/MS, Temperature Converter -40  +125 C +/-1C Serial-I2C, SMBus, 8-Pin MSOP" H 3900 3750 50  0001 L CNN "Description"
F 5 "1.1" H 3900 3650 50  0001 L CNN "Height"
F 6 "Microchip" H 3900 3550 50  0001 L CNN "Manufacturer_Name"
F 7 "MCP9808T-E/MS" H 3900 3450 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "579-MCP9808T-E/MS" H 3900 3350 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=579-MCP9808T-E%2FMS" H 3900 3250 50  0001 L CNN "Mouser Price/Stock"
F 10 "7709896P" H 3900 3150 50  0001 L CNN "RS Part Number"
F 11 "http://uk.rs-online.com/web/p/products/7709896P" H 3900 3050 50  0001 L CNN "RS Price/Stock"
F 12 "70389091" H 3900 2950 50  0001 L CNN "Allied_Number"
F 13 "https://www.alliedelec.com/microchiptechnologyinc-mcp9808t-e-ms/70389091/" H 3900 2850 50  0001 L CNN "Allied Price/Stock"
	1    2950 3850
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J7
U 1 1 5D781E96
P 4800 3950
F 0 "J7" H 4828 3926 50  0000 L CNN
F 1 "Conn_01x04_Female" H 4828 3835 50  0000 L CNN
F 2 "" H 4800 3950 50  0001 C CNN
F 3 "~" H 4800 3950 50  0001 C CNN
	1    4800 3950
	1    0    0    -1  
$EndComp
Text Label 2400 3950 0    50   ~ 0
I2C_SCL
Text Label 2400 3850 0    50   ~ 0
I2C_SDA
Wire Wire Line
	2400 3850 2950 3850
Wire Wire Line
	2400 3950 2950 3950
$Comp
L power:GND #PWR04
U 1 1 5D7854FA
P 2400 4050
F 0 "#PWR04" H 2400 3800 50  0001 C CNN
F 1 "GND" H 2405 3877 50  0000 C CNN
F 2 "" H 2400 4050 50  0001 C CNN
F 3 "" H 2400 4050 50  0001 C CNN
	1    2400 4050
	0    -1   -1   0   
$EndComp
Text Label 4600 3850 2    50   ~ 0
VCC
Text Label 4600 3950 2    50   ~ 0
VCC
Text Label 4600 4050 2    50   ~ 0
VCC
Text Label 4600 4150 2    50   ~ 0
VCC
$Comp
L power:GND #PWR05
U 1 1 5D7A8F22
P 2950 4150
F 0 "#PWR05" H 2950 3900 50  0001 C CNN
F 1 "GND" H 2955 3977 50  0000 C CNN
F 2 "" H 2950 4150 50  0001 C CNN
F 3 "" H 2950 4150 50  0001 C CNN
	1    2950 4150
	0    1    1    0   
$EndComp
Wire Wire Line
	4050 3950 4600 3950
Wire Wire Line
	4050 3850 4600 3850
Wire Wire Line
	4050 4050 4600 4050
Wire Wire Line
	4050 4150 4600 4150
NoConn ~ 2950 4050
$Comp
L Connector:Conn_01x03_Male J5
U 1 1 5D7AC8A3
P 2200 3950
F 0 "J5" H 2300 4300 50  0000 C CNN
F 1 "Conn_01x03_Male" H 2300 4200 50  0000 C CNN
F 2 "" H 2200 3950 50  0001 C CNN
F 3 "~" H 2200 3950 50  0001 C CNN
	1    2200 3950
	1    0    0    -1  
$EndComp
Wire Notes Line
	1950 3400 1950 4300
Wire Notes Line
	1950 4300 5650 4300
Wire Notes Line
	5650 4300 5650 3400
Wire Notes Line
	5650 3400 1950 3400
Text Notes 3150 3400 0    50   ~ 0
Temperature sensor\n
Text Notes 2650 4650 0    50   ~ 0
Connector for the MPU9250 
$Comp
L Connector:Conn_01x06_Female J8
U 1 1 5D7D7961
P 2850 5150
F 0 "J8" H 2878 5126 50  0000 L CNN
F 1 "Conn_01x06_Female" H 2878 5035 50  0000 L CNN
F 2 "" H 2850 5150 50  0001 C CNN
F 3 "~" H 2850 5150 50  0001 C CNN
	1    2850 5150
	-1   0    0    1   
$EndComp
Wire Wire Line
	3050 4850 3400 4850
Text Label 3150 4850 0    50   ~ 0
AD0
$Comp
L power:GND #PWR09
U 1 1 5D7DB224
P 3400 4850
F 0 "#PWR09" H 3400 4600 50  0001 C CNN
F 1 "GND" H 3405 4677 50  0000 C CNN
F 2 "" H 3400 4850 50  0001 C CNN
F 3 "" H 3400 4850 50  0001 C CNN
	1    3400 4850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3050 4950 3400 4950
Text Label 3150 4950 0    50   ~ 0
FSYNC
$Comp
L power:GND #PWR010
U 1 1 5D7DC45C
P 3400 4950
F 0 "#PWR010" H 3400 4700 50  0001 C CNN
F 1 "GND" H 3405 4777 50  0000 C CNN
F 2 "" H 3400 4950 50  0001 C CNN
F 3 "" H 3400 4950 50  0001 C CNN
	1    3400 4950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3050 5050 3650 5050
Wire Wire Line
	3050 5150 3650 5150
Text Label 3650 5050 0    50   ~ 0
I2C_SDA
Text Label 3650 5150 0    50   ~ 0
I2C_SCL
$Comp
L power:GND #PWR08
U 1 1 5D7DDA8C
P 3050 5350
F 0 "#PWR08" H 3050 5100 50  0001 C CNN
F 1 "GND" H 3055 5177 50  0000 C CNN
F 2 "" H 3050 5350 50  0001 C CNN
F 3 "" H 3050 5350 50  0001 C CNN
	1    3050 5350
	0    -1   -1   0   
$EndComp
Text Label 3050 5250 0    50   ~ 0
VCC
Wire Notes Line
	2050 4650 2050 5500
Wire Notes Line
	2050 5500 4150 5500
Wire Notes Line
	4150 5500 4150 4650
Wire Notes Line
	4150 4650 2050 4650
$Comp
L Connector:Conn_01x08_Female J9
U 1 1 5D715A78
P 7000 3900
F 0 "J9" H 7028 3876 50  0000 L CNN
F 1 "Conn_01x08_Female" H 7028 3785 50  0000 L CNN
F 2 "" H 7000 3900 50  0001 C CNN
F 3 "~" H 7000 3900 50  0001 C CNN
	1    7000 3900
	-1   0    0    1   
$EndComp
Text Label 7200 3500 0    50   ~ 0
VCC
$Comp
L power:GND #PWR011
U 1 1 5D71B5E5
P 7200 3600
F 0 "#PWR011" H 7200 3350 50  0001 C CNN
F 1 "GND" H 7205 3427 50  0000 C CNN
F 2 "" H 7200 3600 50  0001 C CNN
F 3 "" H 7200 3600 50  0001 C CNN
	1    7200 3600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7200 3700 7500 3700
Text Label 7500 3700 0    50   ~ 0
MOSI
Wire Wire Line
	7200 3800 7500 3800
Text Label 7500 3800 0    50   ~ 0
MISO
Text Notes 6850 3350 0    50   ~ 0
Connector for AT86RF233\n
Text Label 7500 3900 0    50   ~ 0
SCK
Wire Wire Line
	7200 3900 7500 3900
Wire Wire Line
	7200 4000 7500 4000
Text Label 7500 4000 0    50   ~ 0
SEL_AT86
Text Label 7500 4100 0    50   ~ 0
SLP_TR
Wire Wire Line
	7200 4100 7500 4100
Wire Wire Line
	7200 4200 7500 4200
Wire Notes Line
	6150 4300 8300 4300
Wire Notes Line
	8300 4300 8300 3350
Wire Notes Line
	8300 3350 6150 3350
Wire Notes Line
	6150 3350 6150 4300
$Comp
L LM385:LM385BZ-2.5G D2
U 1 1 5D73AE50
P 7900 2550
F 0 "D2" H 8350 2815 50  0000 C CNN
F 1 "LM385BZ-2.5G" H 8350 2724 50  0000 C CNN
F 2 "TO-92_(TO-226)" H 8650 2650 50  0001 L CNN
F 3 "http://www.onsemi.com/pub/Collateral/LM285-D.PDF" H 8650 2550 50  0001 L CNN
F 4 "ON Semiconductor LM385BZ-2.5G, Fixed Shunt Voltage Reference 2.5V, +/-1.5 % 3-Pin, TO-92" H 8650 2450 50  0001 L CNN "Description"
F 5 "ON Semiconductor" H 8650 2250 50  0001 L CNN "Manufacturer_Name"
F 6 "LM385BZ-2.5G" H 8650 2150 50  0001 L CNN "Manufacturer_Part_Number"
F 7 "863-LM385BZ-2.5G" H 8650 2050 50  0001 L CNN "Mouser Part Number"
F 8 "https://www.mouser.com/Search/Refine.aspx?Keyword=863-LM385BZ-2.5G" H 8650 1950 50  0001 L CNN "Mouser Price/Stock"
F 9 "7743486P" H 8650 1850 50  0001 L CNN "RS Part Number"
F 10 "http://uk.rs-online.com/web/p/products/7743486P" H 8650 1750 50  0001 L CNN "RS Price/Stock"
F 11 "70099811" H 8650 1650 50  0001 L CNN "Allied_Number"
F 12 "http://www.alliedelec.com/on-semiconductor-lm385bz-2-5g/70099811/" H 8650 1550 50  0001 L CNN "Allied Price/Stock"
	1    7900 2550
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x07_Female J10
U 1 1 5D73F6AD
P 8950 3800
F 0 "J10" H 8842 3275 50  0000 C CNN
F 1 "Conn_01x07_Female" H 8842 3366 50  0000 C CNN
F 2 "" H 8950 3800 50  0001 C CNN
F 3 "~" H 8950 3800 50  0001 C CNN
	1    8950 3800
	-1   0    0    1   
$EndComp
$Comp
L Device:R R1
U 1 1 5D74133D
P 7450 2850
F 0 "R1" H 7520 2896 50  0000 L CNN
F 1 "47k" H 7520 2805 50  0000 L CNN
F 2 "" V 7380 2850 50  0001 C CNN
F 3 "~" H 7450 2850 50  0001 C CNN
	1    7450 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5D741A69
P 7650 2550
F 0 "R2" V 7443 2550 50  0000 C CNN
F 1 "1k" V 7534 2550 50  0000 C CNN
F 2 "" V 7580 2550 50  0001 C CNN
F 3 "~" H 7650 2550 50  0001 C CNN
	1    7650 2550
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5D741F5F
P 8350 2100
F 0 "R3" V 8143 2100 50  0000 C CNN
F 1 "330k" V 8234 2100 50  0000 C CNN
F 2 "" V 8280 2100 50  0001 C CNN
F 3 "~" H 8350 2100 50  0001 C CNN
	1    8350 2100
	0    1    1    0   
$EndComp
Wire Wire Line
	7450 2700 7450 2650
Wire Wire Line
	7450 2650 7850 2650
Wire Wire Line
	7450 2650 7450 2550
Wire Wire Line
	7450 2550 7500 2550
Connection ~ 7450 2650
Wire Wire Line
	7800 2550 7850 2550
Wire Wire Line
	8500 2100 8800 2100
Wire Wire Line
	8800 2100 8800 2550
Wire Wire Line
	8800 2550 8900 2550
Connection ~ 8800 2550
$Comp
L power:GND #PWR013
U 1 1 5D747C4F
P 8900 2550
F 0 "#PWR013" H 8900 2300 50  0001 C CNN
F 1 "GND" H 8905 2377 50  0000 C CNN
F 2 "" H 8900 2550 50  0001 C CNN
F 3 "" H 8900 2550 50  0001 C CNN
	1    8900 2550
	0    -1   -1   0   
$EndComp
Text Label 7450 3000 3    50   ~ 0
VCC
Wire Wire Line
	7850 2650 7850 3050
Wire Wire Line
	7850 3050 9350 3050
Wire Wire Line
	9350 3050 9350 3500
Wire Wire Line
	9350 3500 9150 3500
Connection ~ 7850 2650
Wire Wire Line
	7850 2650 7900 2650
Text Label 8350 3050 0    50   ~ 0
VAUX_2.7V
Wire Wire Line
	7850 2550 7850 2100
Wire Wire Line
	7850 2100 8200 2100
Connection ~ 7850 2550
Wire Wire Line
	7850 2550 7900 2550
$Comp
L power:GND #PWR014
U 1 1 5D758BD0
P 9150 3600
F 0 "#PWR014" H 9150 3350 50  0001 C CNN
F 1 "GND" H 9155 3427 50  0000 C CNN
F 2 "" H 9150 3600 50  0001 C CNN
F 3 "" H 9150 3600 50  0001 C CNN
	1    9150 3600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9150 3700 9400 3700
Wire Wire Line
	9150 3800 9400 3800
Wire Wire Line
	9150 3900 9400 3900
Wire Wire Line
	9150 4000 9400 4000
Wire Wire Line
	9150 4100 9400 4100
Text Label 9400 3800 0    50   ~ 0
MOSI
Text Label 9400 3900 0    50   ~ 0
MISO
Text Label 9400 3700 0    50   ~ 0
SEL_AX50
Text Label 9400 4000 0    50   ~ 0
SCK
Text Label 9400 4100 0    50   ~ 0
USUP
Text Notes 8900 3200 0    50   ~ 0
Connector for AX5043\n
Wire Notes Line
	8600 3200 8600 4200
Wire Notes Line
	8600 4200 9850 4200
Wire Notes Line
	9850 4200 9850 3200
Wire Notes Line
	9850 3200 8600 3200
Text Notes 1350 6550 0    50   ~ 0
MOSI: cable to MCU is common\nMISO: cable to MCU is common\nSCK: cable to MCU is common\n\nSEL: 2 different cables\n
Text Label 7500 4200 0    50   ~ 0
VCC
$EndSCHEMATC
